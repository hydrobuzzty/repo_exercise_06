﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace CoAHomework
{
    public class Spawner : MonoBehaviour
    {
        public int spawnCounter;
        
        private void Loop()
        {
            for (int i = 0; i < spawnCounter; i++)
            {
                GameObject newGo = new GameObject();

                Debug.Log("Something spawned!!!");
            }
        }

        private void AnotherSpawner(int enemySpawn)
        {
            while (enemySpawn < spawnCounter)
            {
                GameObject newGo = new GameObject();

                Debug.Log("Something spawned!!!");
                enemySpawn++;

            }
        }

        public void Start()
        {
            Loop();
            
        }
    }
}
